package mams;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Module;


public class JSONConverter extends Module {

    ObjectMapper objectMapper = new ObjectMapper();

    @TERM
    public JsonNode parse(String json){
        try{
            JsonNode node = objectMapper.readTree(json);
            System.out.println("NODE: "+node);
            return node;
        } catch(JsonParseException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    //JsonNode node = json.parse(json-string)
    //json.valueAsString(node, )


    public JsonNode getNode(JsonNode node, String path){
        String split[] = path.split("/");
        JsonNode temp = node;
        for(String s: split){
            if(s.isEmpty()) continue;
            int index  = s.indexOf("[");
            if(index == -1){
                temp = temp.get(s);
            }else{
                if(index > 0){
                    String segment = s.substring(0, index);
                    temp = temp.get(segment); 
                }
                String rank = s.substring(index+1, s.length()-1);
                temp = temp.get(Integer.parseInt(rank));
            }
        }
        return temp;
    }

    @TERM
    public ObjectNode createObjectNode(){
        ObjectNode node = objectMapper.createObjectNode();
        return node;
    }

    public void addField(ObjectNode objecNode, String key, String value){
        objecNode.put(key, value);
    }

    @TERM
    public String toJsonString(ObjectNode objectNode){
        return objectNode.toString();
    }

    @TERM
    public String valueAsString(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText();
    }

    @TERM
    public Boolean valueAsBoolean(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asBoolean();
    }

    @TERM
    public Integer valueAsInteger(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asInt();
    }

    @TERM
    public Double valueAsDouble(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asDouble();
    }

    @TERM
    public Long valueAsLong(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asLong();
    }
}
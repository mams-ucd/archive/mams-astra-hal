package mams;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Module;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;

public class HALConverter extends Module {
    private static final String HAL_PREFIX = "_";

    ObjectMapper mapper = new ObjectMapper();

    @TERM
    public Funct toHalFunct(String json) {
        try {
            JsonNode tree = mapper.readTree(json);
            Term[] terms = new Term[tree.size()];

            int i = 0;
            Iterator<Entry<String, JsonNode>> it = tree.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                terms[i++] = createTerm(field.getKey(), field.getValue());
            }
            return new Funct("hal", terms);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @TERM
    public Funct toRawFunct(String predicate, String json) {
        try {
            JsonNode tree = mapper.readTree(json);

            // Calculate the number of non_hal fields
            int size = countNonHalFields(tree);
            Term[] terms = new Term[size];

            int i = 0;
            Iterator<Entry<String, JsonNode>> it = tree.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                if (!field.getKey().startsWith(HAL_PREFIX)) {
                    if (field.getValue().isObject()) {
                        terms[i++] = new Funct(field.getKey(), new Term[] {createTerm(field.getKey(), field.getValue()) });;
                    } else {
                        terms[i++] = new Funct(field.getKey(), new Term[] {createTerm(field.getValue()) });;
                    }
                }
            }
            return new Funct(predicate, terms);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @TERM
    public Object toObject(String json, String type) {
        try {
            return mapper.readValue(json, Class.forName(type));
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int countNonHalFields(JsonNode tree) {
        int size = tree.size();
        Iterator<String> keyIt = tree.fieldNames();
        while (keyIt.hasNext()) {
            if (keyIt.next().startsWith(HAL_PREFIX))
                size--;
        }
        return size;
    }

    private Term createTerm(JsonNode node) {
        if (node.isArray()) {
            ListTerm items = new ListTerm();
            Iterator<JsonNode> it = node.elements();
            while (it.hasNext()) {
                items.add(createTerm(it.next()));
            }
            return items;
        } else if (node.size() == 0) {
            if (node.isLong()) {
                return Primitive.newPrimitive(node.asLong());
            } else if (node.isInt()) {
                return Primitive.newPrimitive(node.asInt());
            } else if (node.isDouble()) {
                return Primitive.newPrimitive(node.asDouble());
            } else if (node.isFloat()) {
                return Primitive.newPrimitive(node.floatValue());
            } else if (node.isBoolean()) {
                return Primitive.newPrimitive(node.asBoolean());
            } else if (node.isTextual()) {
                return Primitive.newPrimitive(node.asText());
            }
        }
        throw new RuntimeException("[HALConverter] Could not convert: " + node);
    }

    private Term createTerm(String key, JsonNode node) {
        Term[] terms = null;
        if (node.size() == 0) {
            terms = new Term[1];
            if (node.isLong()) {
                terms[0] = Primitive.newPrimitive(node.asLong());
            } else if (node.isInt()) {
                terms[0] = Primitive.newPrimitive(node.asInt());
            } else if (node.isDouble()) {
                terms[0] = Primitive.newPrimitive(node.asDouble());
            } else if (node.isFloat()) {
                terms[0] = Primitive.newPrimitive(node.floatValue());
            } else if (node.isBoolean()) {
                terms[0] = Primitive.newPrimitive(node.asBoolean());
            } else if (node.isTextual()) {
                terms[0] = Primitive.newPrimitive(node.asText());
            } else if (node.isArray()) {
                System.out.println("Converting array: " + key);
                ListTerm items = new ListTerm();
                Iterator<Entry<String, JsonNode>> it = node.fields();
                while (it.hasNext()) {
                    Entry<String, JsonNode> field = it.next();
                    items.add(createTerm(field.getKey(), field.getValue()));
                }
                System.out.println("List: " + items);
                return items;
            } else {
                throw new RuntimeException("[HALConverter] Could not convert: " + node);
            }
        } else {
            terms = new Term[node.size()];
            int i = 0;
            Iterator<Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                terms[i++] = createTerm(field.getKey(), field.getValue());
            }
        }

        return new Funct(key, terms);
    }

    /**
     * Outer function name is stripped for JSON generation
     * 
     * @param function
     * @return
     */
    @TERM
    public String toJsonString(Funct function) {
        try {
            return mapper.writeValueAsString(createJson(function));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    
    private ObjectNode createJson(Funct function) {
        ObjectNode node  = mapper.createObjectNode();
        for (Term term : function.terms()) {
            if (Funct.class.isInstance(term)) {
                Funct funct = (Funct) term;
                Term value = funct.termAt(0);
                if (Funct.class.isInstance(value)) {
                    node.set(funct.functor(), createJson((Funct) value));
                } else if (ListTerm.class.isInstance(value)) {
                    node.set(funct.functor(), createJson((ListTerm) value));
                } else if (Primitive.class.isInstance(value)) {
                    addPrimitiveField(node, funct.functor(), value);
                }
            }
        }
        return node;
    }

    private ArrayNode createJson(ListTerm list) {
        ArrayNode node = mapper.createArrayNode();
        for (Term term : list) {
            if (Funct.class.isInstance(term)) {
                node.add(createJson((Funct) term));
            } else if (Primitive.class.isInstance(term)) {
                addPrimitive(node, term);
            }
        }
        return node;
    }

    @SuppressWarnings("unchecked")
    private void addPrimitive(ArrayNode node, Term value) {
        if (value.type().equals(Type.BOOLEAN)) {
            node.add(((Primitive<Boolean>) value).value());
        } else if (value.type().equals(Type.CHAR)) {
            node.add(((Primitive<Character>) value).value().toString());
        } else if (value.type().equals(Type.DOUBLE)) {
            node.add(((Primitive<Double>) value).value());
        } else if (value.type().equals(Type.FLOAT)) {
            node.add(((Primitive<Float>) value).value());
        } else if (value.type().equals(Type.INTEGER)) {
            node.add(((Primitive<Integer>) value).value());
        } else if (value.type().equals(Type.LONG)) {
            node.add(((Primitive<Long>) value).value());
        } else if (value.type().equals(Type.STRING)) {
            node.add(((Primitive<String>) value).value());
        } else {
            throw new RuntimeException("Unknown type: " + value.type());
        }
    }

    @SuppressWarnings("unchecked")
    private void addPrimitiveField(ObjectNode node, String key, Term value) {
        if (value.type().equals(Type.BOOLEAN)) {
            node.put(key, ((Primitive<Boolean>) value).value());
        } else if (value.type().equals(Type.CHAR)) {
            node.put(key, ((Primitive<Character>) value).value().toString());
        } else if (value.type().equals(Type.DOUBLE)) {
            node.put(key, ((Primitive<Double>) value).value());
        } else if (value.type().equals(Type.FLOAT)) {
            node.put(key, ((Primitive<Float>) value).value());
        } else if (value.type().equals(Type.INTEGER)) {
            node.put(key, ((Primitive<Integer>) value).value());
        } else if (value.type().equals(Type.LONG)) {
            node.put(key, ((Primitive<Long>) value).value());
        } else if (value.type().equals(Type.STRING)) {
            node.put(key, ((Primitive<String>) value).value());
        } else {
            throw new RuntimeException("Unknown type: " + value.type());
        }
    }

    @TERM public ListTerm getFields(String schema) {
        Class<?> cls = null;
        try {
            cls = Class.forName(schema);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        ListTerm list = new ListTerm();
        for (Field field : cls.getFields()) {
            list.add(Primitive.newPrimitive(field.getName()));
        }
        return list;
    }
}
package fipa.artifact;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.ARTIFACT_INFO;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.WebServer;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class Inbox extends ResourceArtifact {
    private static ObjectMapper mapper = new ObjectMapper();

    {
        name = "inbox";
    }

    @OPERATION
    public void receive(String performative, String sender, String content) {
        signal("message", performative, sender, content);
    }

    @Override
    public boolean handle(ChannelHandlerContext ctxt, FullHttpRequest request) {
        if (!request.method().asciiName().toString().equals("POST")) {
            WebServer.writeErrorResponse(ctxt, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            try {
                JsonNode node = mapper.readTree(Utils.getBody(request));
                CartagoBackend.getInstance().doAction(this.getId(), new Op("receive", node.get("performative").asText(),
                        node.get("sender").asText(), mapper.writeValueAsString(node.get("content"))));
                WebServer.writeResponse(ctxt, request, HttpResponseStatus.OK, "application/json", "");
            } catch (Exception e) {
                WebServer.writeResponse(ctxt, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text",
                        e.getMessage());
                e.printStackTrace();
            }
        }

        return false;
    }

}
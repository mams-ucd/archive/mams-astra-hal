package mams;

agent MAMSAgent {
	module Cartago cartago;
    module System S;
    module Functions F;
    module mams.HALConverter hal;

    types mams_core {
        formula have(string);
        formula created(string);
        formula artifact(string, string, cartago.ArtifactId);
        formula itemResource(string, string);
        formula listResource(string, string);
        formula schema(string, string);
        formula agentUri(string);
    }

    inference have(string name) :- artifact(name, string qualifiedName, cartago.ArtifactId id);
    inference created(string qualifiedName) :- artifact(string name, qualifiedName, cartago.ArtifactId id);
    
    // ****************************************************************************
    // PLANS FOR SETTING UP A MAMS AGENT / SERVICE
    // ****************************************************************************

    /**
     * General plan for initialising a MAMS agent
     */
    rule +!init() {
        cartago.link();
        !have("webserver");
        !have("restclient");
    }    

    rule +!have(string name) : ~have(name) {
        cartago.lookupArtifact(name, cartago.ArtifactId id);
        +artifact(name, name, id);
    }

    /**
     * Special plan to initialise the platform. It should be used only
     * once.  Will fail if used twice on the same JVM.
     */
	rule +!setup() {
        !setup(9000);
    }

    rule +!setup(int port_number) {
		cartago.startService();
		cartago.link();
        
		cartago.makeArtifact("webserver", "mams.artifacts.WebServerArtifact", cartago.params([port_number]), cartago.ArtifactId id);
        +artifact("webserver", "webserver", id);

        cartago.makeArtifact("restclient", "mams.artifacts.RESTArtifact", cartago.params([]), cartago.ArtifactId id2);
        +artifact("restclient", "restclient", id2);

        cartago.makeArtifact("comms", "fipa.artifact.Comms", cartago.params([]), cartago.ArtifactId id3);
        +artifact("comms", "comms", id3);
    }

    // ****************************************************************************
    // PLANS FOR INTERACTING WITH OTHER SERVICES
    // ****************************************************************************
    
    rule +!post(string uri, string jsonBody, int responseCode, string content) : artifact("restclient", "restclient", cartago.ArtifactId id) {
        cartago.operation(id, postRequest(uri, jsonBody, responseCode, content));
    }

    rule +!put(string uri, string jsonBody, int responseCode, string content) : artifact("restclient", "restclient", cartago.ArtifactId id) {
        cartago.operation(id, putRequest(uri, jsonBody, responseCode, content));
    }

    rule +!get(string uri, int responseCode, string content) : artifact("restclient", "restclient", cartago.ArtifactId id) {
        cartago.operation(id, getRequest(uri,  responseCode, content));
    }

    // DELETE to follow...

    rule +!created("base") : ~created("base") & artifact("webserver", string qualifiedName, cartago.ArtifactId id2) {
        string baseName = S.name()+"-base";

        cartago.makeArtifact(baseName, "mams.artifacts.BaseArtifact", cartago.params([S.name()]), cartago.ArtifactId id);
        cartago.linkArtifacts(id, "out-1", id2);
        cartago.focus(id);
        cartago.operation(id, createRoute());
        +artifact("base", baseName, id);
	}	

    // ****************************************************************************
    // PLANS FOR FIPA AGENT COMMUNICATION
    // ****************************************************************************

    rule +!created("inbox") : ~have("inbox") & artifact("base", string qualifiedName, cartago.ArtifactId id2) {
        string baseName = qualifiedName+"-inbox";

        cartago.makeArtifact(baseName, "fipa.artifact.Inbox", cartago.params([]), cartago.ArtifactId id);
        cartago.linkArtifacts(id, "out-1", id2);
        cartago.focus(id);
        cartago.operation(id, createRoute());

        +artifact("inbox", baseName, id);
    }

    rule $cartago.signal(string sa, message(string performative, string sender, string content)) {
        !signal_message(performative, sender, hal.toRawFunct("content", content));
    }

    rule +!signal_message(string performative, string sender, content(funct content)) {
        !message(performative, sender, content);
    }

    rule +!transmit(string performative, string receiver, funct content) : artifact("comms", string qualifiedName, cartago.ArtifactId id) {
        !itemProperty("base", "uri", funct agentUri);
        cartago.operation(id, transmit(performative, F.valueAsString(agentUri, 0), receiver, hal.toJsonString(content(content))));
    }

    // ****************************************************************************
    // REASONING ABOUT SCHEMA...
    // ****************************************************************************
    rule +!schema(string schema) : ~schema(schema, string X) {
        list fields = hal.getFields(schema);
        forall (string field : fields) {
            +schema(schema, field);
        }
    }

    // ****************************************************************************
    // ACCESSING RESOURCE PROPERTIES...
    // ****************************************************************************
    rule +!itemProperty(string artifact_name, string property, funct value) : artifact(artifact_name, string qname, cartago.ArtifactId aid) {
        cartago.operation(aid, observeProperty(property, cartago.ArtifactObsProperty prop));
        value = cartago.toFunction(prop);
    }
}
